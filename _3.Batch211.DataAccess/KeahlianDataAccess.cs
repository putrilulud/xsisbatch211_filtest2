﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _2.Batch211.ViewModel;
using _4.Batch211.DataModel;

namespace _3.Batch211.DataAccess
{
    public class KeahlianDataAccess
    {
        public static List<KeahlianViewModel> GetListAll()
        {
            List<KeahlianViewModel> result = new List<KeahlianViewModel>();
            using (var db = new DB_Xsis_Point_TwoEntities())
                {
                    result = (from keahllian in db.x_keahlian.Where(a => a.is_delete == false)
                              join attKeahlianLevel in db.x_skill_level
                              on keahllian.skill_level_id equals attKeahlianLevel.id

                              select new KeahlianViewModel
                              {
                                  id = keahllian.id,
                                  skill_name = keahllian.skill_name,
                                  skill_level_id = attKeahlianLevel.id,
                                  notes = keahllian.notes,
                                  created_by = keahllian.created_by,
                                  created_on = keahllian.created_on,
                                  is_delete = keahllian.is_delete,
                                  biodata_id = keahllian.biodata_id,
                                  KeahlianName = attKeahlianLevel.name
                              }
                              ).ToList();
                }
            return result;
        }

        public static KeahlianViewModel GetDetailsById(long id)
        {
            KeahlianViewModel result = new KeahlianViewModel();
            using (var db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from attributs in db.x_keahlian
                          where attributs.id == id
                          select new KeahlianViewModel
                          {
                              id = attributs.id,
                              skill_name = attributs.skill_name,
                              skill_level_id = attributs.skill_level_id,
                              notes = attributs.notes,
                              created_by = attributs.created_by,
                              created_on = attributs.created_on,
                              is_delete = attributs.is_delete,
                              biodata_id = attributs.biodata_id
                          }).FirstOrDefault();
            }
            return result;
        }

        public static string Message = string.Empty;
        public static bool Insert(KeahlianViewModel model)
        {
            bool result = true;
            try
            {
                using (var db = new DB_Xsis_Point_TwoEntities())
                {
                    x_keahlian attributs = new x_keahlian();
                    attributs.skill_name = model.skill_name;
                    attributs.skill_level_id = model.skill_level_id;
                    attributs.notes = model.notes;
                    attributs.created_by = model.created_by;
                    attributs.created_on = model.created_on;
                    attributs.is_delete = model.is_delete;
                    attributs.biodata_id = model.biodata_id;

                    db.x_keahlian.Add(attributs);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {

                Message = hasError.Message;
                result = false;
            }
            return result;
        }
        public static bool Update(KeahlianViewModel model)
        {
            bool result = true;
            try
            {
                using (var db = new DB_Xsis_Point_TwoEntities())
                {
                    x_keahlian attributs =
                        db.x_keahlian.Where(
                            o => o.id == model.id).FirstOrDefault();

                    if (attributs != null)
                    {
                        attributs.skill_name = model.skill_name;
                        attributs.skill_level_id = model.skill_level_id;
                        attributs.notes = model.notes;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Keahlian not found!";
                    }
                }
            }
            catch (Exception hasError)
            {

                Message = hasError.Message;
                result = false;
            }
            return result;
        }
        public static bool Delete(KeahlianViewModel model)
        {
            bool result = true;
            try
            {
                using (var db = new DB_Xsis_Point_TwoEntities())
                {
                    x_keahlian attributs = db.x_keahlian.Where(
                            o => o.id == model.id).FirstOrDefault();

                    if (attributs != null)
                    {
                        attributs.is_delete = true;
                        //attributs.skill_name = model.skill_name;
                        //attributs.skill_level_id = model.skill_level_id;
                        //attributs.notes = model.notes;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Variant not found!";
                    }
                }
            }
            catch (Exception hasError)
            {

                Message = hasError.Message;
                result = false;
            }
            return result;
        }

        //combobox Skill Level
        public static List<SkillLevelViewModel> KeahlianCollection()
        {
            List<SkillLevelViewModel> listKeahlian = new List<SkillLevelViewModel>();
            using (var db = new DB_Xsis_Point_TwoEntities())
            {
                listKeahlian = (from Attribute in db.x_skill_level
                                  where Attribute.is_delete == false
                                  select new SkillLevelViewModel
                                  {
                                      id = Attribute.id,
                                      name = Attribute.name
                                  }).ToList();
            }
            return listKeahlian;
        }
    }
}

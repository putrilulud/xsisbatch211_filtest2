﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using _2.Batch211.ViewModel;
using _4.Batch211.DataModel;

namespace _3.Batch211.DataAccess
{
    public class DokumenDataAccess
    {
        public static string Massage = string.Empty;

        //method untuk mendapat semua isi table
        public static List<DokumenViewModel> GetListAll()
        {
            List<DokumenViewModel> result = new List<DokumenViewModel>();
            using (var db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from attributs in db.x_biodata_attachment
                          select new DokumenViewModel
                          {
                              id = attributs.id,
                              created_by = attributs.created_by,
                              created_on = attributs.created_on,
                              is_delete = attributs.is_delete,
                              biodata_id = attributs.biodata_id,
                              file_name = attributs.file_name,
                              file_path = attributs.file_path,
                              notes = attributs.notes
                          }
                          ).ToList();
            }
            return result;
        }
        // method untuk data secara spesifik (berupa row)
        public static DokumenViewModel getDetailsById(long id)
        {
            DokumenViewModel result = new DokumenViewModel();
            using (var db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from attributs in db.x_biodata_attachment
                          where attributs.id == id
                          select new DokumenViewModel
                          {
                              id = attributs.id,
                              created_by = attributs.created_by,
                              created_on = attributs.created_on,
                              is_delete = attributs.is_delete,
                              biodata_id = attributs.biodata_id,
                              file_name = attributs.file_name,
                              file_path = attributs.file_path,
                              notes = attributs.notes
                          }
                          ).FirstOrDefault();
            }
            return result;
        }
        //method untuk insert
        public static bool Insert(DokumenViewModel model)
        {
            bool result = true;
            try
            {

                using (var db = new DB_Xsis_Point_TwoEntities())
                {
                    x_biodata_attachment attributs = new x_biodata_attachment();
                    attributs.created_by = model.created_by;
                    attributs.created_on = DateTime.Now;
                    attributs.is_delete = model.is_delete;
                    attributs.biodata_id = model.biodata_id;
                    attributs.file_name = model.file_name;
                    attributs.file_path = model.file_path;
                    attributs.notes = model.notes;
                    
                    db.x_biodata_attachment.Add(attributs);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {

                Massage = hasError.Message;
                result = false;
            }
            return result;
        }
    }
}

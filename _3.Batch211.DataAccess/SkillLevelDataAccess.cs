﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _2.Batch211.ViewModel;
using _4.Batch211.DataModel;

namespace _3.Batch211.DataAccess
{
    class SkillLevelDataAccess
    {
        public static List<SkillLevelViewModel> GetListAll()
        {
            List<SkillLevelViewModel> result = new List<SkillLevelViewModel>();
            using (var db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from attributs in db.x_skill_level
                          select new SkillLevelViewModel
                          {
                              id = attributs.id,
                              name = attributs.name,
                              is_delete = attributs.is_delete,
                              description = attributs.description,
                              created_by = attributs.created_by,
                              created_on = DateTime.Now,
                              modified_by = attributs.modified_by,
                              modified_on = DateTime.Now
                          }
                         ).ToList();
            }
            return result;
        }
    }
}

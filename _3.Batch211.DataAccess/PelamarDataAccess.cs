﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _2.Batch211.ViewModel;
using _4.Batch211.DataModel;

namespace _3.Batch211.DataAccess
{
    public class PelamarDataAccess
    {
        public static List<PelamarViewModel> GetListAll()
        {
            List<PelamarViewModel> result = new List<PelamarViewModel>();
            using (DB_Xsis_Point_TwoEntities db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from pelamar in db.x_biodata
                          join pendidikan in db.x_riwayat_pendidikan
                          on pelamar.id equals pendidikan.biodata_id
                          select new PelamarViewModel
                          {
                              id = pelamar.id,
                              fullname = pelamar.fullname,
                              nick_name = pelamar.nick_name,
                              email = pelamar.email,
                              phone_number1 = pelamar.phone_number1,
                              school_name = pendidikan.school_name,
                              major = pendidikan.major
                          }).ToList();
            }
            return result;
        }
        public static List<PelamarViewModel> Search(string kataKunci)
        {
            List<PelamarViewModel> result = new List<PelamarViewModel>();
            using (DB_Xsis_Point_TwoEntities db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from pelamar in db.x_biodata
                        join pendidikan in db.x_riwayat_pendidikan
                        on pelamar.id equals pendidikan.biodata_id
                        where pelamar.fullname.Contains(kataKunci)

                        select new PelamarViewModel
                        {
                            id = pelamar.id,
                            fullname = pelamar.fullname,
                            nick_name = pelamar.nick_name,
                            email = pelamar.email,
                            phone_number1 = pelamar.phone_number1,
                            school_name = pendidikan.school_name,
                            major = pendidikan.major
                        }).ToList();
            }
            return result;
        }
        public static List<PelamarViewModel> OrderByAsc(string kataKunci)
        {
            List<PelamarViewModel> result = new List<PelamarViewModel>();
            using (DB_Xsis_Point_TwoEntities db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from pelamar in db.x_biodata
                          join pendidikan in db.x_riwayat_pendidikan
                          on pelamar.id equals pendidikan.biodata_id
                          where pelamar.fullname.Contains(kataKunci)
                          orderby pelamar.fullname ascending

                          select new PelamarViewModel
                          {
                              id = pelamar.id,
                              fullname = pelamar.fullname,
                              nick_name = pelamar.nick_name,
                              email = pelamar.email,
                              phone_number1 = pelamar.phone_number1,
                              school_name = pendidikan.school_name,
                              major = pendidikan.major
                          }).ToList();
            }
            return result;
        }
        public static List<PelamarViewModel> OrderByDesc(string kataKunci)
        {
            List<PelamarViewModel> result = new List<PelamarViewModel>();
            using (DB_Xsis_Point_TwoEntities db = new DB_Xsis_Point_TwoEntities())
            {
                result = (from pelamar in db.x_biodata
                          join pendidikan in db.x_riwayat_pendidikan
                          on pelamar.id equals pendidikan.biodata_id
                          where pelamar.fullname.Contains(kataKunci)
                          orderby pelamar.fullname descending

                          select new PelamarViewModel
                          {
                              id = pelamar.id,
                              fullname = pelamar.fullname,
                              nick_name = pelamar.nick_name,
                              email = pelamar.email,
                              phone_number1 = pelamar.phone_number1,
                              school_name = pendidikan.school_name,
                              major = pendidikan.major
                          }).ToList();
            }
            return result;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _3.Batch211.DataAccess;
using _2.Batch211.ViewModel;

namespace _1.Batch211.Web.Controllers
{
    public class DokumenController : Controller
    {
        // GET: Dokumen
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {

            return PartialView("List", DokumenDataAccess.GetListAll());
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        public JsonResult UploadFiles(DokumenViewModel model)
        {
            model.created_by = 1;
            model.created_on = DateTime.Now;
            model.is_delete = false;
            model.biodata_id = 1;

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    // create the uploads folder if it doesn't exist
                    Directory.CreateDirectory(Server.MapPath("~/UploadedFiles/"));
                    string path = Path.Combine(Server.MapPath("~/UploadedFiles/"), fileName);

                    // save the file
                    file.SaveAs(path);
                    return Json("File uploaded successfully");

                }

                catch (Exception e)
                {
                    return Json("error" + e.Message);
                }
            }
            return Json("no files were selected !");
            
        }
    }
}
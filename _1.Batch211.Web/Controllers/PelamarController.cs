﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using _2.Batch211.ViewModel;
using _3.Batch211.DataAccess;

namespace _1.Batch211.Web.Controllers
{
    public class PelamarController : Controller
    {
        // GET: Pelamar
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(string kataKunci, int? page)
        {
            
            return PartialView("_List", PelamarDataAccess.Search(kataKunci));

        }
        public ActionResult OrderByAsc(string kataKunci)
        {
            return PartialView("_List", PelamarDataAccess.OrderByAsc(kataKunci));
        }
        public ActionResult OrderByDesc(string kataKunci)
        {
            return PartialView("_List", PelamarDataAccess.OrderByDesc(kataKunci));
        }

    }
}
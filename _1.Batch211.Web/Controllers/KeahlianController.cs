﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _3.Batch211.DataAccess;
using _2.Batch211.ViewModel;

namespace _1.Batch211.Web.Controllers
{
    public class KeahlianController : Controller
    {
        // GET: Keahlian
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            
            return PartialView("List", KeahlianDataAccess.GetListAll());
        }
        public ActionResult Create()
        {
            ViewBag.KeahlianList = KeahlianDataAccess.KeahlianCollection();
            //ViewBag.CategoriesList = Models.Helper.ToSelectList(KeahlianDataAccess.CategoriesCollection(), "Id", "Name");
            return PartialView("_Create");
        }
        [HttpPost]
        public ActionResult Create(KeahlianViewModel model)
        {
            model.created_by = 1;
            model.created_on = DateTime.Now;
            model.is_delete = false;
            model.biodata_id = 1;
        
            if (KeahlianDataAccess.Insert(model))
            {
                return Json(new
                {
                    success = true,
                    message = "Berhasil"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = KeahlianDataAccess.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(long id)
        {
            ViewBag.KeahlianList = KeahlianDataAccess.KeahlianCollection();
            return PartialView("_Edit", KeahlianDataAccess.GetDetailsById(id));
        }

        [HttpPost]
        public ActionResult SaveEdit(KeahlianViewModel model)
        {
            if (KeahlianDataAccess.Update(model))
            {
                return Json(new { success = true, message = "berhasil" },
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = KeahlianDataAccess.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(long id)
        {
            ViewBag.KeahlianList = KeahlianDataAccess.KeahlianCollection();
            return PartialView("_Delete", KeahlianDataAccess.GetDetailsById(id));
        }

        [HttpPost]
        public ActionResult SaveDelete(KeahlianViewModel model)
        {
            model.created_by = 1;
            model.created_on = DateTime.Now;
            model.is_delete = false;
            model.biodata_id = 1;

            if (KeahlianDataAccess.Delete(model))
            {
                return Json(new
                {
                    success = true,
                    message = "Berhasil"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = KeahlianDataAccess.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
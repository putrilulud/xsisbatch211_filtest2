﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Batch211.ViewModel
{
    public class PelamarViewModel
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<int> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_deleted { get; set; }

        [DisplayName("Nama Lengkap")]
        public string fullname { get; set; }

        [DisplayName("Nama Panggilan")]
        public string nick_name { get; set; }

        [DisplayName("Tempat Lahir")]
        public string pob { get; set; }
        public System.DateTime dob { get; set; }

        [DisplayName("Jenis Kelamin")]
        public bool gender { get; set; }

        [DisplayName("Agama")]
        public long religion_id { get; set; }

        [DisplayName("Tinggi Badan")]
        public Nullable<int> high { get; set; }

        [DisplayName("Berat Badan")]
        public Nullable<int> weight { get; set; }

        [DisplayName("Kewarganegaraan")]
        public string nationality { get; set; }

        [DisplayName("Suku Bangsa")]
        public string ethnic { get; set; }

        [DisplayName("Kegemaran/Hobi")]
        public string hobby { get; set; }

        [DisplayName("Jenis Identitas")]
        public long identity_type_id { get; set; }

        [DisplayName("Nomor Identitas")]
        public string identity_no { get; set; }

        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("No. HP")]
        public string phone_number1 { get; set; }

        [DisplayName("No. HP Alternatif")]
        public string phone_number2 { get; set; }

        [DisplayName("No. Tlp(Rumah/Orang Tua)")]
        public string parent_phone_number { get; set; }

        [DisplayName("Anak Ke")]
        public string child_sequence { get; set; }

        [DisplayName("Dari Berapa Bersaudara")]
        public string how_many_brothers { get; set; }

        [DisplayName("Status Pernikahan")]
        public long martial_status_id { get; set; }

        public Nullable<long> addrbook_id { get; set; }

        public string token { get; set; }
        public Nullable<System.DateTime> expired_token { get; set; }

        [DisplayName("Tahun Pernikahan")]
        public string marrige_year { get; set; }
        public long company_id { get; set; }
        public Nullable<bool> is_process { get; set; }
        public Nullable<bool> is_complete { get; set; }

        public long biodata_id { get; set; }
        public string school_name { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public Nullable<long> education_level_id { get; set; }
        public string entry_year { get; set; }
        public string graduation_year { get; set; }
        public string major { get; set; }
        public Nullable<decimal> gpa { get; set; }
        public string notes { get; set; }
        public Nullable<int> order { get; set; }
        public string judul_ta { get; set; }
        public string deskripsi_ta { get; set; }

    }
}

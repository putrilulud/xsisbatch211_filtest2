﻿

namespace _2.Batch211.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    public class DokumenViewModel
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_delete { get; set; }
        public long biodata_id { get; set; }
        [DisplayName("Nama File")]
        public string file_name { get; set; }
        [DisplayName("Upload File")]
        public string file_path { get; set; }
        [DisplayName("Keterangan")]
        public string notes { get; set; }
        public Nullable<bool> is_photo { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Batch211.ViewModel
{
    public class KeahlianViewModel
    {
        public long id { get; set; }

        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_delete { get; set; }
        public long biodata_id { get; set; }
        [DisplayName("Keahlian")]
        public string skill_name { get; set; }
        [DisplayName("Level Keahlian")]
        public Nullable<long> skill_level_id { get; set; }
        [DisplayName("Keterangan")]
        public string notes { get; set; }
        [DisplayName("Lvl Keahlian")]
        public string KeahlianName { get; set; }
    }
}
